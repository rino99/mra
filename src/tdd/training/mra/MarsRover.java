package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	private int planetX;
	private int planetY;
	List<String> planetObstacles = new ArrayList<>(); 
	private String status;
	private int statusX;
	private int statusY;
	private char dir;
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if(planetX != 0 && planetY != 0) {
			this.planetX = planetX;
			this.planetY = planetY;
			this.planetObstacles = planetObstacles;
			this.status = "(0,0,N)";
			this.statusX = 0;
			this.statusY = 0;
			this.dir = 'N';
		}else {
			throw new MarsRoverException();
		}
		
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(inRange(x,y)) {
			String planetObstacle = "(" + x + "," + y + ")";
			for(int i = 0; i < planetObstacles.size(); i++) {
				if(compareObstacle(i, planetObstacle)) {
					return true;
				}
			}
			return false;
		}else {
			throw new MarsRoverException();
		}
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		this.statusX = 5;
		this.statusY = 8;
		this.dir = 'E';
		String ostacle = "";
		
		for(int i = 0; i < commandString.length(); i++) {
			moveRover(commandString.charAt(i));
			createStatus();
			String statustemp = "(" + this.statusX + "," + this.statusY  +")";
			for(int j = 0; j < planetObstacles.size(); j++) {
				if(statustemp.equals(planetObstacles.get(j))) {
					backwordStep();
					if(!ostacle.contains(planetObstacles.get(j))) {
						ostacle += planetObstacles.get(j);
					}
				}
			}
		}
		
		shouldWrappingX();
		shouldWrappingY();
		if(ostacle != "") {
			crateStatusWithObstacle(ostacle);
		}else {
			createStatus();
		}
		return this.status;
	}

	private boolean compareObstacle(int i, String planetObstacle) {
		return planetObstacles.get(i).equals(planetObstacle);
	}
	
	private boolean isEmptyCommand(char c) {
		return c == ' ';
	}
	
	private boolean isRightCommand(char c) {
		return c == 'r';
	}
	
	private boolean isLeftCommand(char c) {
		return c == 'l';
	}
	
	private void createStatus() {
		this.status = "(" + this.statusX + "," + this.statusY + "," + this.dir + ")";
	}
	
	private void crateStatusWithObstacle(String tmp) {
		this.status = "(" + this.statusX + "," + this.statusY + "," + this.dir + ")" + tmp;
	}
	
	private void backwordStep() {
		if(this.dir == 'N') {
			this.statusY = this.statusY - 1;
		}else if(this.dir == 'S') {
			this.statusY = this.statusY + 1;
		}else if(this.dir == 'E') {
			this.statusX = this.statusX - 1;
		}else if(this.dir == 'W') {
			this.statusX = this.statusX + 1;
		}
	}
	
	private void forwardStep() {
		if(this.dir == 'N') {
			this.statusY = this.statusY + 1;
		}else if(this.dir == 'S') {
			this.statusY = this.statusY - 1;
		}else if(this.dir == 'E') {
			this.statusX = this.statusX + 1;
		}else if(this.dir == 'W') {
			this.statusX = this.statusX - 1;
		}
	}
	
	private void turnRight() {
		if(this.dir == 'N') {
			this.dir = 'E';
		}else if(this.dir == 'S') {
			this.dir = 'W';
		}else if(this.dir == 'E') {
			this.dir = 'S';
		}else if(this.dir == 'W') {
			this.dir = 'N';
		}
	}
	
	private void turnLeft() {
		if(this.dir == 'N') {
			this.dir = 'W';
		}else if(this.dir == 'S') {
			this.dir = 'E';
		}else if(this.dir == 'E') {
			this.dir = 'N';
		}else if(this.dir == 'W') {
			this.dir = 'S';
		}
	}
	
	private void moveRover(char c) throws MarsRoverException{
		if(isEmptyCommand(c)) {
			createStatus();
		}else if(isRightCommand(c)){
			turnRight();
		}else if(isLeftCommand(c)){
			turnLeft();
		}else if(c == 'f'){
			forwardStep();
		}else if(c == 'b'){
			backwordStep();
		}else {
			throw new MarsRoverException();
		}
	}
	
	public void shouldWrappingX(){
		if(this.statusX == 10) {
			this.statusX = 0;
		}
	}
	
	public void shouldWrappingY(){
		if(this.statusY == 10) {
			this.statusY = 0;
		}
	}
	
	private boolean inRange(int x, int y) {
		return (x < this.planetX && y < this.planetY);
	}
	
}
