package tdd.training.mra;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;


public class MarsRoverTest {

	@Test
	public void testPlanetContainsObstacleAt() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertTrue(marsRover.planetContainsObstacleAt(5,5));
	}
	@Test
	public void testPlanetContainsObstacleAtTriangolation() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertTrue(marsRover.planetContainsObstacleAt(7,8));
	}
	
	@Test
	public void testRoverExecuteCommandEmpty() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(5,8,E)",marsRover.executeCommand(""));
	}
	
	@Test(expected = MarsRoverException.class)
	public void testRoverExecuteCommandException() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(5,8,E)",marsRover.executeCommand("casual"));
	}
	
	@Test
	public void testRoverExecuteCommandRight() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(5,8,S)",marsRover.executeCommand("r"));
	}
	
	@Test
	public void testRoverExecuteCommandLeft() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(5,8,N)",marsRover.executeCommand("l"));
	}
	
	@Test
	public void testRoverMovingForward() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(6,8,E)",marsRover.executeCommand("f"));
	}
	
	@Test
	public void testRoverMovingBackword() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(4,8,E)",marsRover.executeCommand("b"));
	}
	
	@Test
	public void testRoverMovingCombined() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(7,6,S)",marsRover.executeCommand("ffrff"));
	}
	
	@Test
	public void testRoverWrappingX() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(0,8,E)",marsRover.executeCommand("fffff"));
	}
	
	@Test
	public void testRoverWrappingY() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(7,8)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(5,0,N)",marsRover.executeCommand("lff"));
	}
	
	@Test
	public void testRoverSingleObstacle() throws MarsRoverException {
		List<String> planetObstacles = new ArrayList<>(); 
		planetObstacles.add("(5,5)");
		planetObstacles.add("(9,9)");
		MarsRover marsRover = new MarsRover(10,10, planetObstacles);
		assertEquals("(8,9,E)(9,9)",marsRover.executeCommand("lfrffff"));
	}
}
